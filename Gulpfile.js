// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint') ,
  	spawn = require('child_process').spawn ,
    exec = require('child_process').exec ,
    help = require('gulp-task-listing') ,
    node ;

// Add a task to render the output
gulp.task('help', help);

gulp.task('server', function() {
  if (node) node.kill('SIGINT');
  // node = spawn('node', ['server.js'], {stdio: 'inherit'});
  node = spawn('npm', ['run', 'debug'], {stdio: 'inherit'});

  setInterval( function () {
    console.log('Sending SIGINT');
    if (node) node.kill('SIGINT');
  }, 1000);
  node.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
});

// Lint Task
gulp.task('lint', function() {
    gulp.src(['**/*.js', '!node_modules/**/*.js', '!public/components/**/*.js'])
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('default'));
});

gulp.task('start', ['server'], function() {

  gulp.watch(['./**/*.js', '!./node_modules/**/*'
  	], function() {
        gulp.run('server');
  });

  // Need to watch for sass changes too? Just add another watch call!
  // no more messing around with grunt-concurrent or the like. Gulp is
  // async by default.
});

// clean up if an error goes unhandled.
process.on('exit', function() {
    if (node) node.kill();
});
