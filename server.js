var mitch = require('mitch');
var app = mitch('mitch-example');

var errorHandler = require('errorhandler');
var sequelizeDebug = require('debug')('sequelize:mitch');

app.initialize();

// Feel free to add new stuff here

app.loadModules(); // Loads all modules defined in config.modules
app.loadUserModules(); // Loads all modules defined under ./modules
app.loadModels(); // Actually loads models from app._models into sequelize and builds associations

app.syncModels()(function (err) {
  if (err) {
    console.log (err);
  } else {
    sequelizeDebug('models synced');
  }
});

// Or here

// Perhaps a fancy 404 handler here

// And something to handle errors (just in case)
app.use(errorHandler());

app.start();
