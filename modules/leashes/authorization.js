
module.exports = [
  {
    'accessType': '*', // ['*', 'GET', 'POST', 'PUT', 'DELETE'] // Based on method type because that's how it would probably be implemented
    'accessObject': '', // Can include nested objects . delimited, ie, leashes.owner
    'function': '$authenticated' // A function which has been registered
  }
]


// Loopback:

// "acls": [
//   {
//     "accessType": "*",
//     "principalType": "ROLE",
//     "principalId": "$everyone",
//     "permission": "DENY"
//   },
//   {
//     "accessType": "READ",
//     "principalType": "ROLE",
//     "principalId": "sessionAuthenticated",
//     "permission": "ALLOW"
//   },
//   {
//     "accessType": "WRITE",
//     "principalType": "ROLE",
//     "principalId": "sessionAuthenticated",
//     "permission": "ALLOW"
//   }
// ]