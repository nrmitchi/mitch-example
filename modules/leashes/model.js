
var debug = require('debug')('example:leashes:model');

module.exports = function (app) {
  return function(sequelize, DataTypes) {
    var Leash = sequelize.define('Leash', {  
      length : {
        type : DataTypes.INTEGER ,
        allowNull : false ,
        validate : {
        }
      } , 
      color : {
        type : DataTypes.STRING ,
        allowNull : false ,
        validate : {
        }
      }
    }, {
      paranoid: true ,
      tableName: 'leashes' ,
      comment: 'Leashes database' ,
      associate: function(models) {
        // models.User.hasMany(Leash, { foreignKey: 'user_id' })
        models.Pet.hasMany(Leash, { foreignKey: 'pet_id' });
        // Leash.belongsTo(models.Pet, { foreignKey: 'pet_id' })
      },
      classMethods: {
        // attributeWhitelist: function(){ return [] }
      },
      instanceMethods: {
        // getFullname: function() {
        //   return [this.first_name, this.last_name].join(' ');
        // }
      }
    });

    Leash.beforeValidate( function(leash, options, fn) {
      debug('beforeValidate - Leash');
      return fn(null, leash);
    });
    Leash.afterValidate( function(leash, options, fn) {
      debug('afterValidate - Leash');
      return fn(null, leash);
    });
    Leash.beforeCreate( function(leash, options, fn) {
      debug('beforeCreate - Leash');
      return fn();
    });
    Leash.afterCreate( function(leash, options, fn) {
      debug('afterCreate - Leash');
      return fn();
    });
    Leash.beforeUpdate( function(leash, options, fn) {
      debug('beforeUpdate - Leash');
      return fn();
    });
    Leash.afterUpdate( function(leash, options, fn) {
      debug('afterUpdate - Leash');
      return fn();
    });
    Leash.beforeDestroy( function(leash, options, fn) {
      debug('beforeDestroy - Leash');
      return fn();
    });
    Leash.afterDestroy( function(leash, options, fn) {
      debug('afterDestroy - Leash');
      return fn();
    });

    return Leash;
  };
};
