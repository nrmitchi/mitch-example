
module.exports = function (app) {

  // Push models to be loaded
  app._models.push({
    module  : 'Leashes' ,
    Factory : require('./model')(app)
  });

  return {
    mountPath  : '/leashes' ,
    server     : require('./server.js')(app)
  };
};
