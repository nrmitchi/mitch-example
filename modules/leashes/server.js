
var _           = require('lodash') ;

module.exports = function (app) {
  var router      = app.Router('Leashes') ,
      Controller  = require('./controller')(app, 'Leash') ,
      Blueprints  = require('mitch-blueprints')(app, 'Leash') ;

  router.param('leash_id', Blueprints.loadMiddleware );

  var permissionCheck = Blueprints.permissionCheck('Leash');
  
  router.get('/',             permissionCheck, Controller.index );
  router.get('/:leash_id',    permissionCheck, Controller.show );

  router.post('/',            permissionCheck, Controller.create );

  router.delete('/:leash_id', permissionCheck, Controller.delete );

  return router;
};