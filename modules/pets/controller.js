// Note: I should generate all the errors I will return, and make a code mapping for all of them

var fs         = require('fs') ,
    _          = require('lodash') ;

module.exports = function (app, modelName) {

  // Because the models wont be loaded until after things are mounted,
  // I have to pass the model name in. Kjnd of weird, but what can ya do
  var Blueprints = require('mitch-blueprints')(app, 'Pet') ;

  return {
    index   : Blueprints.find ,
    show    : Blueprints.findOne ,
    create  : Blueprints.create ,
    update  : Blueprints.notImplemented ,
    destroy : Blueprints.destroy
  };
};
