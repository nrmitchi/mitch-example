
module.exports = function (app) {

  // Push models to be loaded
  app._models.push({
    module  : 'Pets' ,
    Factory : require('./model')(app)
  });

  return {
    mountPath  : '/pets' ,
    server     : require('./server.js')(app)
  };
};
