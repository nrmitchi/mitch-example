
var _           = require('lodash') ;

module.exports = function (app) {
  var router      = app.Router('Pets') ,
      Controller  = require('./controller')(app, 'Pet') ,
      Blueprints  = require('mitch-blueprints')(app, 'Pet') ;

  router.param( 'pet_id', Blueprints.loadMiddleware );

  router.get('/',             Controller.index );
  router.get('/:pet_id',      Controller.show );

  router.post('/',            Controller.create );

  router.delete('/:pet_id',   Controller.destroy );

  router.use('/:pet_id/leashes', Blueprints.relations('Leash') );

  return router;
};