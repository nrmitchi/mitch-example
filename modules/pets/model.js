
var debug = require('debug')('example:pets:model');

module.exports = function (app) {
  return function(sequelize, DataTypes) {
    var Pet = sequelize.define('Pet', {  
      name : {
        type : DataTypes.STRING ,
        allowNull : false ,
        validate : {
        }
      } , 
      type : {
        type : DataTypes.STRING ,
        allowNull : false ,
        validate : {
        }
      } ,
      birthdate : {
        type : DataTypes.DATE ,
        allowNull : false ,
        validate : {
        }
      }
    }, {
      paranoid: true ,
      tableName: 'pets' ,
      comment: 'Pets database' ,
      associate: function(models) {
        // models.User.hasMany(Pet, { foreignKey: 'user_id' })
        // Pet.belongsTo(models.User, { foreignKey: 'user_id' })
      },
      classMethods: {
        // attributeWhitelist: function(){ return [] }
      },
      instanceMethods: {
        // getFullname: function() {
        //   return [this.first_name, this.last_name].join(' ');
        // }
      }
    });

    Pet.beforeValidate( function(pet, options, fn) {
      debug('beforeValidate - Pet');
      return fn(null, pet);
    });
    Pet.afterValidate( function(pet, options, fn) {
      debug('afterValidate - Pet');
      return fn(null, pet);
    });
    Pet.beforeCreate( function(pet, options, fn) {
      debug('beforeCreate - Pet');
      return fn();
    });
    Pet.afterCreate( function(pet, options, fn) {
      debug('afterCreate - Pet');
      return fn();
    });
    Pet.beforeUpdate( function(pet, options, fn) {
      debug('beforeUpdate - Pet');
      return fn();
    });
    Pet.afterUpdate( function(pet, options, fn) {
      debug('afterUpdate - Pet');
      return fn();
    });
    Pet.beforeDestroy( function(pet, options, fn) {
      debug('beforeDestroy - Pet');
      return fn();
    });
    Pet.afterDestroy( function(pet, options, fn) {
      debug('afterDestroy - Pet');
      return fn();
    });

    return Pet;
  };
};
