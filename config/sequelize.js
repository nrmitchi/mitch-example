
var debug = require('debug')('sequelize:queries');

module.exports = {
  host: 'localhost',
  username: 'nrmitchi',
  password: '',
  database: 'mitch-example',
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  logging: debug
};
