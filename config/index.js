
module.exports = {
  host: 'mitch.dev',
  port: process.env.PORT || 2000,
  protocol: 'http'
};
