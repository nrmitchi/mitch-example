
/**
 * Payment Configuration
 *
 * Currently this payment module only integrates with Stripe.
 *
 * https://www.petekeen.net/stripe-webhook-event-cheatsheet
 *
 */

module.exports = {

  stripe_api_key: process.env.STRIPE_API_KEY ,
  // Which stripe events to monitor
  monitoredEvents: ['charge.succeeded'] ,

  validPlans: ['free', 'starter', 'pro'] ,

  defaultPlan: 'free'

};
