
module.exports = {
  host: process.env.STATSD_HOST || 'services.nrmitchi.com',
  port: process.env.STATSD_PORT
};
