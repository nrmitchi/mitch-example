
module.exports = {
  host: process.env.NSQ_HOST ,
  port: process.env.NSQ_PORT
};
